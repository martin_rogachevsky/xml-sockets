package server.controllers;

import infrastructure.InputParser;
import infrastructure.Response;
import infrastructure.ResponseCode;
import infrastructure.interfaces.IController;

public final class RunController implements IController{
    /**
     * Method for handling a command, that comes as {@param command}.
     * Method returns {@return} response that can be handled by View-system.
     *
     * @param command
     */
    @Override
    public Response handleCommand(String command) {
        if (command == null) return new Response(ResponseCode.Fail, null, "Enter the command!");
        String action = InputParser.getAction(command);
        switch (action){
            case "quit":
                return back();
            default:
                return new Response(ResponseCode.Fail, null, (action + " - is not a command!"));
        }
    }

    /**
     * Method to handle 'come-back' type of command.
     *
     * @return is the response that means coming back.
     */
    @Override
    public Response back() {
        return null;
    }
}
