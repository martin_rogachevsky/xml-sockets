package server.controllers;

import infrastructure.InputParser;
import infrastructure.Response;
import infrastructure.ResponseCode;
import infrastructure.interfaces.IController;

/**
 * Controller that handles commands from menu view.
 */
public final class MenuController implements IController {
    /**
     * Method for handling a command, that comes as {@param command}.
     * Method returns {@return} response that can be handled by View-system.
     *
     * @param command
     */
    @Override
    public Response handleCommand(String command) {
        if (command == null) return new Response(ResponseCode.Fail, null, "Enter the command!");
        String action = InputParser.getAction(command);
        switch (action){
            case "back":
                return back();
            case "run":
                return new Response(ResponseCode.Redir, "run", null);
            default:
                return new Response(ResponseCode.Fail, null, (action + " - is not a command!"));
        }
    }

    /**
     * Method to handle 'come-back' type of command.
     *
     * @return is the response that means coming back.
     */
    @Override
    public Response back() {
        return new Response(ResponseCode.Fail, null,  "Moving back from menu is impossible! Use 'quit' to finish.");
    }
}
