package server.services;

import java.net.ServerSocket;
import java.net.Socket;

/**
 * Service that listens for a client connection.
 */
public class ListenerService
{
    /**
     * Socket for a listening.
     */
    private ServerSocket serverSocket;

    /**
     * Part that listener is bound to.
     */
    private int port;

    public ListenerService(int port){
        this.port = port;
    }

    /**
     * Opening a listener socket.
     */
    public void startListen(){
        try {
            serverSocket = new ServerSocket(port);
            System.out.println("Server started.");

        } catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * Retrieving a client-connected socket, that is ready to communication with client.
     * @return - connected socket.
     */
    public Socket getConnectedSocket(){
        try {
            Socket s = serverSocket.accept();
            System.out.println("Connection established.");
            return s;
        } catch (Exception e){
            System.out.println(e.getMessage());
            return null;
        }
    }

    public void close(){
        try {
            serverSocket.close();
        } catch (Exception e){}
    }
}
