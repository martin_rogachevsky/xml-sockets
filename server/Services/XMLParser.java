package server.services;

import domain.EducationType;
import domain.Specialization;
import domain.Student;
import infrastructure.net.ParserType;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.events.*;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

/**
 * Class for parsing Xml with one of the DOM, SAX or StAX parsers.
 */
class XMLParser {
    /**
     * Type of the parser.
     */
    private final ParserType type;

    XMLParser(ParserType type){
        this.type = type;
    }

    /**
     * Parsing Xml-file with DOM-parser.
     * @param xml - file for parsing.
     * @return - specialization nested in the file.
     * @throws Exception might be occurred by parser.
     */
    private Specialization parseDom (String xml) throws Exception{
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8.name())));

        Specialization specialization = new Specialization();
        Element root = doc.getDocumentElement();
        specialization.id = Integer.parseInt(root.getElementsByTagName("id").item(0).getTextContent());
        specialization.name = root.getElementsByTagName("name").item(0).getTextContent();
        specialization.type = EducationType.valueOf(root.getElementsByTagName("type").item(0).getTextContent());
        NodeList students = ((Element)root.getElementsByTagName("students").item(0)).getElementsByTagName("student");
        for (int i = 0; i < students.getLength(); i++) {
            Element element = (Element) students.item(i);
            Student student = new Student();
            student.id = Integer.parseInt(element.getElementsByTagName("id").item(0).getTextContent());
            student.name = element.getElementsByTagName("name").item(0).getTextContent();
            NodeList specList = element.getElementsByTagName("specializationList").item(0).getChildNodes();
            student.specializations = new int[specList.getLength()];
            for (int j = 0; j < specList.getLength(); j++){
                student.specializations[j] = Integer.parseInt(specList.item(j).getNodeValue());
            }
        }
        return specialization;
    }

    /**
     * Parsing Xml-file with SAX-parser.
     * @param xml - file for parsing.
     * @return - specialization nested in the file.
     * @throws Exception might be occurred by parser.
     */
    private Specialization parseSax(String xml) throws Exception{
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();
        Specialization specialization = new Specialization();

        DefaultHandler handler = new DefaultHandler() {
            Student currentStudent;
            boolean studentOpened = false;
            boolean specListOpened = false;
            boolean specInSpecListOpened = false;
            String elementName;
            ArrayList<Integer> specList;

            @Override
            public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
                elementName = qName;
                if (qName.equalsIgnoreCase("student")) {
                    studentOpened = true;
                    currentStudent = new Student();
                }
                if (qName.equalsIgnoreCase("specializationList")) {
                    specListOpened = true;
                    specList = new ArrayList<Integer>();
                }
                if (specListOpened){
                    specInSpecListOpened = true;
                }
            }

            @Override
            public void characters(char ch[], int start, int length) throws SAXException {
                if (elementName.equalsIgnoreCase("id")){
                    if (studentOpened)
                        currentStudent.id = Integer.parseInt(new String(ch, start, length));
                    else
                        specialization.id = Integer.parseInt(new String(ch, start, length));
                }
                if (elementName.equalsIgnoreCase("name")){
                    if (studentOpened)
                        currentStudent.name = new String(ch, start, length);
                    else
                        specialization.name = new String(ch, start, length);
                }
                if (elementName.equalsIgnoreCase("type"))
                    specialization.type = EducationType.valueOf(new String(ch, start, length));
                if (specInSpecListOpened)
                    specList.add(Integer.parseInt(new String(ch, start, length)));

            }

            @Override
            public void endElement(String uri, String localName, String qName) throws SAXException {
                if (qName.equalsIgnoreCase("student")){
                    specialization.students.add(currentStudent);
                    currentStudent = null;
                    studentOpened = false;
                }
                if (qName.equalsIgnoreCase("specializationList")) {
                    specListOpened = false;
                    currentStudent.specializations = new int[specList.size()];
                    for (int i = 0;i<specList.size();i++){
                        currentStudent.specializations[i]=specList.get(i);
                    }
                    specList = null;
                }
                if (specInSpecListOpened)
                    specListOpened = false;
            }

        };

        InputStream stream = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8.name()));
        saxParser.parse(stream, handler);
        return specialization;
    }

    /**
     * Parsing Xml-file with StAX-parser.
     * @param xml - file for parsing.
     * @return - specialization nested in the file.
     * @throws Exception might be occurred by parser.
     */
    private Specialization parseStAX(String xml) throws Exception{
        Specialization specialization = new Specialization();
        Student currentStudent = new Student();
        boolean studentOpened = false;
        boolean specListOpened = false;
        boolean specInSpecListOpened = false;
        String elementName = "";
        ArrayList<Integer> specList = new ArrayList<>();

        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLEventReader eventReader =
                factory.createXMLEventReader(new StringReader(xml));

        String qName;
        while (eventReader.hasNext()) {
            XMLEvent event = eventReader.nextEvent();

            switch(event.getEventType()) {

                case XMLStreamConstants.START_ELEMENT:
                    StartElement startElement = event.asStartElement();
                    qName = startElement.getName().getLocalPart();

                    elementName = qName;
                    if (qName.equalsIgnoreCase("student")) {
                        studentOpened = true;
                        currentStudent = new Student();
                    }
                    if (qName.equalsIgnoreCase("specializationList")) {
                        specListOpened = true;
                        specList = new ArrayList<Integer>();
                    }
                    if (specListOpened){
                        specInSpecListOpened = true;
                    }
                    break;

                case XMLStreamConstants.CHARACTERS:
                    Characters characters = event.asCharacters();
                    if (elementName.equalsIgnoreCase("id")){
                        if (studentOpened)
                            currentStudent.id = Integer.parseInt(characters.getData());
                        else
                            specialization.id = Integer.parseInt(characters.getData());
                    }
                    if (elementName.equalsIgnoreCase("name")){
                        if (studentOpened)
                            currentStudent.name = characters.getData();
                        else
                            specialization.name = characters.getData();
                    }
                    if (elementName.equalsIgnoreCase("type"))
                        specialization.type = EducationType.valueOf(characters.getData());
                    if (specInSpecListOpened)
                        specList.add(Integer.parseInt(characters.getData()));
                    break;

                case XMLStreamConstants.END_ELEMENT:
                    EndElement endElement = event.asEndElement();
                    qName = endElement.getName().getLocalPart();

                    if (qName.equalsIgnoreCase("student")){
                        specialization.students.add(currentStudent);
                        currentStudent = null;
                        studentOpened = false;
                    }
                    if (qName.equalsIgnoreCase("specializationList")) {
                        specListOpened = false;
                        currentStudent.specializations = new int[specList.size()];
                        for (int i = 0;i<specList.size();i++){
                            currentStudent.specializations[i]=specList.get(i);
                        }
                        specList = null;
                    }
                    if (specInSpecListOpened)
                        specListOpened = false;
                    break;
            }
        }

        return specialization;
    }

    /**
     * Choosing parser and parsing Xml file.
     * @param xml - file for parsing.
     * @return - specialization nested in the file.
     * @throws Exception might be occurred by parser.
     */
    Specialization parse(String xml) throws Exception{
        switch (type) {
            case SAX:
                return parseSax(xml);
            case StAX:
                return parseStAX(xml);
            case DOM:
                return parseDom(xml);
            default:
                return null;
        }
    }

}
