package server.services;

import com.google.gson.Gson;
import infrastructure.net.Response;
import infrastructure.net.Request;
import infrastructure.net.ResponseStatus;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

/**
 * Class for handling accepted connection.
 */
public class ConnectionHandlerService {

    /**
     * Opened socket for communication with a client.
     */
    private Socket socket;

    public ConnectionHandlerService(Socket socket){
        this.socket = socket;
    }

    /**
     * Handling connection by receiving request, getting result and sending response.
     */
    public void HandleConnection(){
        try (BufferedReader inFromClient =
                     new BufferedReader(new InputStreamReader(socket.getInputStream()));
             BufferedWriter outFromServer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()))){
            StringBuilder clientRequestBuilder = new StringBuilder();
            while (inFromClient.ready()){
                clientRequestBuilder.append(inFromClient.readLine());
            }
            System.out.println("Request is received.");
            String clientRequest = clientRequestBuilder.toString();
            Gson gson = new Gson();
            Request request = gson.fromJson(clientRequest, Request.class);
            Response response;
            try{
                RequestHandler handler = new RequestHandler();
                response = handler.handleRequest(request);
            } catch (Exception e){
                System.out.println("While handling request from client: "+e.getMessage());
                response = new Response();
                response.status = ResponseStatus.ServerError;
            }
            String serverResponse = gson.toJson(response);
            outFromServer.write(serverResponse);
            System.out.println("Response is sent.");
            socket.close();
            System.out.println("Socket is closed.");
        } catch (Exception e){
            System.out.println("While handling connection from client: "+e.getMessage());
        }
    }

    public void close(){
        try {
            socket.close();
        } catch (Exception e){}
    }

}
