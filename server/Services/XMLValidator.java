package server.services;

import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.StringReader;

/**
 * XML validator. Provides XML validation via XSD-scheme for Specialization domain class.
 */
class XMLValidator {
    /**
     * XSD-file stream-source.
     */
    private final Source xsdSchemeSource;

    XMLValidator() throws Exception{
        xsdSchemeSource = loadXsdScheme();
    }

    /**
     * Loading XML file source.
     * @return - XML file source.
     * @throws Exception might be occurred by file reading errors.
     */
    private Source loadXsdScheme() throws Exception{
        File xsdFile = (new File("scheme.xsd"));
        Source source;
        try(FileReader fileReader = new FileReader(xsdFile);
            BufferedReader bufferedReader = new BufferedReader(fileReader)){
            source = new StreamSource(bufferedReader);
        }
        return source;
    }

    /**
     * Method for checking whether XMl-file matches XSD-scheme or not.
     * @param xml - XML-file that is supposed to be checked.
     * @return - true if XML file is valid. false - if not.
     * @throws Exception might be occurred by creating validator errors.
     */
    boolean isSpecializationXmlValid(String xml) throws Exception{
        Source xmlFile = new StreamSource(new StringReader(xml));
        SchemaFactory schemaFactory = SchemaFactory
                .newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = schemaFactory.newSchema(xsdSchemeSource);
        Validator validator = schema.newValidator();
        try{
            validator.validate(xmlFile);
        } catch (SAXException e){
            System.out.println("Checked XML file is not valid.");
            return false;
        }
        return true;
    }
}
