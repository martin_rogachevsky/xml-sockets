package server;

import server.controllers.MenuController;
import infrastructure.InputHandler;
import infrastructure.Response;
import infrastructure.interfaces.IView;
import infrastructure.Viewer;
import server.controllers.RunController;
import server.services.ConnectionHandlerService;
import server.services.ListenerService;
import server.views.ErrorView;
import server.views.MenuView;
import server.views.RunView;

import java.util.Stack;

/**
 * Main class of the application.
 */
public class XmlSocketAppServer {
    private static Stack<String> path;
    private static InputHandler handler;
    private static Viewer viewer;
    private static ConnectionHandlerService handlerService;
    private static ListenerService listenerService;
    private static boolean run;


    public static void startServer(){
        listenerService.startListen();
        run = true;
        while (run) {
            handlerService = new ConnectionHandlerService(listenerService.getConnectedSocket());
            handlerService.HandleConnection();
        }
    }

    private static void configure() throws Exception{
        listenerService = new ListenerService(3666);

        //path configuration
        path = new Stack<>();
        path.push("server");
        path.push("menu");


        //InputHandler configuration
        handler = new InputHandler();
        handler.addController("menu", new MenuController());
        handler.addController("run", new RunController());

        //Viewer configuration
        viewer = new Viewer();
        viewer.addView("err", new ErrorView());
        viewer.addView("menu", new MenuView());
        viewer.addView("run", new RunView());

    }

    /**
     * Start point of the application.
     * @param args Arguments from console.
     */
    public static void main(String[] args) {
        try{
            configure();
        }catch (Exception e){
            System.out.println("Configuration error!");
            System.out.println(e.getMessage());
            return;
        }

        boolean stop = true;
        String input;
        Response response;
        IView view = viewer.getView(path.peek(),null);

        while (stop){
            input =  view.show();
            if (input.equalsIgnoreCase("quit")){
                stop = false;
                run = false;
                listenerService.close();
                handlerService.close();
            }
            else{
                input = path.peek() + ";" + input;
                try{
                    response = handler.handleInput(input);
                    view = viewer.getView(response, path);
                }
                catch (Exception e){
                    viewer.getView("err", e).show();
                }
            }
        }
    }
}
