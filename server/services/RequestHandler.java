package server.services;

import infrastructure.net.Request;
import infrastructure.net.Response;
import infrastructure.net.ResponseStatus;

class RequestHandler {
    Response handleRequest(Request request) throws Exception{
        Response response = new Response();
        XMLValidator validator = new XMLValidator();
        if (validator.isSpecializationXmlValid(request.xml)){
            response.status = ResponseStatus.Valid;
            System.out.println("Accepted XML is valid.");
        }
        else {
            response.status = ResponseStatus.InValid;
            System.out.println("Accepted XML is invalid.");
            return response;
        }
        XMLParser parser = new XMLParser(request.parserType);
        response.specialzation = parser.parse(request.xml);
        return response;
    }
}
