package server.views;

import infrastructure.interfaces.IView;
import server.XmlSocketAppServer;

import java.util.Scanner;

public final class RunView implements IView {
    /**
     * Method shows view and retrieves user input.
     *
     * @return - user input.
     */
    @Override
    public String show() {
        XmlSocketAppServer.startServer();
        System.out.println("                   Server is running.");
        System.out.println("====================================================");
        System.out.println(" Server was started. Press 'Enter' key to interrupt.");
        System.out.println("           Log will be displayed here.");
        System.out.println("====================================================");
        try{
            System.in.read();
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
        return "quit";
    }

    /**
     * Method that allows view to get data from outside.
     *
     * @param param - data that used by view.
     */
    @Override
    public void build(Object param) {

    }

    /**
     * Method that resets view.
     */
    @Override
    public void clean() {

    }
}
