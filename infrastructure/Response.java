package infrastructure;

/**
 * Response class represents set of values for transferring data from controller to view-mechanism.
 */
public class Response {

    /**
     * Response code.
     * Helps to define result of person asked action.
     */
    public ResponseCode code;

    /**
     * Redirection link.
     */
    public String link;

    /**
     * Data for transferring.
     */
    public Object param;

    public Response(ResponseCode code, String link, Object param){
        this.code = code;
        this.param = param;
        this.link = link;
    }
}
