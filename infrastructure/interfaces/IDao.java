package infrastructure.interfaces;

import java.util.Collection;

/**
 * Interface for management data of {@param <T>} class (Data Access Object).
 * Retrieving and uploading data from/into physical media.
 */
public interface IDao<T> {

    /**
     * Connection to the media.
     *
     * @param filename - name of the file that consists data.
     *                 After opening connection method will return {@return} configured DAO class.
     * @throws Exception can be caused by filesystem errors.
     */
    IDao openConnection(String filename) throws Exception;

    /**
     * Saving data to the physical media.
     *
     * @throws Exception can be caused by filesystem errors.
     */
    void save() throws Exception;

    /**
     * Retrieving all data from DAO.
     *
     * @return is a certain class HashSet of objects.
     */
    Collection<T> getAll();

    /**
     * Getting object with {@param id} passed as parameter.
     *
     * @return is a required object.
     */
    T get(int id);

    /**
     * Method for adding items to DAO.
     * Item is passing as parameter{@param item}.
     *
     * @return - id of added item.
     */
    int add(T item);

    /**
     * Removing item from DAO.
     * @param id - id of removing item.
     * @return - id of removed item.
     */
    int remove(int id);

    /**
     * Updating item.
     * @param item - item that supposed to be updated.
     * @return - id of updated item.
     */
    int update(T item);
}
