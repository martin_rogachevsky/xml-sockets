package infrastructure.net;

import domain.Specialization;

/**
 * Response from a server.
 */
public class Response {
    /**
     * Indicates whether requested XML-file is valid of not.
     */
    public ResponseStatus status;

    /**
     * Result nested in XML-file.
     */
    public Specialization specialzation;
}
