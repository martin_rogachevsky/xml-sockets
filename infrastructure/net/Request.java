package infrastructure.net;

/**
 * Request from client with XML and type of XML-parser.
 */
public class Request {
    /**
     * XML to be parsed on a server.
     */
    public String xml;

    /**
     * Type of a parser that is going to be used for parsing XML
     */
    public ParserType parserType;
}
