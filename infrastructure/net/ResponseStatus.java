package infrastructure.net;

/**
 * Enum indicates status of response.
 */
public enum ResponseStatus {
    /**
     * Requested XML is valid.
     */
    Valid,

    /**
     * Requested XML is invalid.
     */
    InValid,

    /**
     * Error was occurred while handling request.
     */
    ServerError
}
