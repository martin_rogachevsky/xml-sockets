package infrastructure.net;

/**
 * Enum consists XML-parsers types.
 */
public enum ParserType {
    SAX,
    StAX,
    DOM
}
