package infrastructure;

import java.util.Scanner;

/**
 * Helper class for validation person's input.
 */
public final class InputValidator{

    /**
     * Method checks if input configuration values are valid.
     * @param args - values.
     * @return - true/false.
     */
    public static boolean isConfDataValid(String[] args){
        if (args.length != 3) return false;
        for (int i = 0; i<3; i++){
            if (!isInteger(args[i]))
                return false;
        }
        return true;
    }

    /**
     * Method checks if string is an integer.
     * @param s - string.
     * @return - true/false.
     */
    public static boolean isInteger(String s) {
        Scanner sc = new Scanner(s.trim());
        if(!sc.hasNextInt(10)) return false;
        sc.nextInt(10);
        return !sc.hasNext();
    }

}
