package client.services;

import domain.Student;
import infrastructure.net.Response;
import infrastructure.net.ResponseStatus;

public class ResponseDisplayer {
    private Response response;

    ResponseDisplayer(Response response){
        this.response = response;
    }

    void Display(){
        if (response.status == ResponseStatus.ServerError){
            System.out.println("Error occurred on server while processing XML.");
            return;
        }
        System.out.println("Sent XML-file is "+response.status.toString());
        if (response.status == ResponseStatus.Valid){
            System.out.println("Specialization:");
            System.out.println("-- Id: "+ response.specialzation.id);
            System.out.println("-- Name: "+ response.specialzation.name);
            System.out.println("-- Education type: "+ response.specialzation.type.toString());
            System.out.println("-- Students: ");
            for (Student student: response.specialzation.students) {
                System.out.println("---- Student: ");
                System.out.println("------ Id: " + student.id);
                System.out.println("------ Name: " + student.name);
                System.out.println("------ Specializations:");
                for (int i : student.specializations) {
                    System.out.println("-------- Specialization Id: " + i);
                }
            }
        }
    }
}
