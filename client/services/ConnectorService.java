package client.services;

import com.google.gson.Gson;
import infrastructure.net.ParserType;
import infrastructure.net.Request;
import infrastructure.net.Response;

import java.io.*;
import java.net.Socket;

/**
 * Class for handling accepted connection.
 */
public class ConnectorService {

    /**
     * Opened socket for communication with a server.
     */
    private Socket socket;

    private File xml;

    private ParserType parserType;

    public ConnectorService(String hostname, int port, ParserType parserType) throws Exception{
        xml = new File("speciality.xml");
        this.socket = new Socket(hostname, port);
        System.out.println("Connection established.");
    }

    /**
     * Handling connection by sending request and receiving response.
     */
    public Response Connect(){
        Response response = null;
        try (BufferedReader inFromServer =
                     new BufferedReader(new InputStreamReader(socket.getInputStream()));
             BufferedWriter outFromClient = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()))){

            Request request = new Request();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream("specialization.xml")));
            String line = bufferedReader.readLine();
            request.parserType = parserType;
            StringBuilder sb = new StringBuilder();
            while (line != null){
                sb.append(line).append("\n");
                line = bufferedReader.readLine();
            }
            request.xml = sb.toString();

            Gson gson = new Gson();
            outFromClient.write(gson.toJson(request));
            StringBuilder serverResponseBuilder = new StringBuilder();
            while (inFromServer.ready()) {
                serverResponseBuilder.append(inFromServer.readLine());
            }
            response = gson.fromJson(serverResponseBuilder.toString(),Response.class);

            socket.close();
            System.out.println("Socket is closed.");
        } catch (Exception e){
            System.out.println("While handling connection from server: "+e.getMessage());

        }
        return response;
    }

    public void close(){
        try {
            socket.close();
        } catch (Exception e){}
    }

}
