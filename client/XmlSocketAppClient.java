package client;

import client.controllers.MenuController;
import client.controllers.RunController;
import client.services.ConnectorService;
import client.views.RunView;
import infrastructure.InputHandler;
import infrastructure.Response;
import infrastructure.interfaces.IView;
import infrastructure.Viewer;
import client.views.ErrorView;
import client.views.MenuView;
import infrastructure.net.ParserType;

import java.util.Stack;

/**
 * Main class of the application.
 */
public class XmlSocketAppClient {
    private static Stack<String> path;
    private static InputHandler handler;
    private static Viewer viewer;
    private static ConnectorService connectorService;


    public static void startClient(){
        connectorService.Connect();
    }

    /**
     * Configuration of application components.
     * @throws Exception Can be caused by opening DAO connections.
     */
    private static void configure() throws Exception{
        connectorService = new ConnectorService(null,3666, ParserType.DOM);

        //path configuration
        path = new Stack<>();
        path.push("client");
        path.push("menu");


        //InputHandler configuration
        handler = new InputHandler();
        handler.addController("menu", new MenuController());
        handler.addController("run", new RunController());

        //Viewer configuration
        viewer = new Viewer();
        viewer.addView("err", new ErrorView());
        viewer.addView("menu", new MenuView());
        viewer.addView("run", new RunView());

    }

    /**
     * Start point of the application.
     * @param args Arguments from console.
     */
    public static void main(String[] args) {
        try{
            configure();
        }catch (Exception e){
            System.out.println("Configuration error!");
            return;
        }

        boolean stop = true;
        String input;
        Response response;
        IView view = viewer.getView(path.peek(),null);

        while (stop){
            input =  view.show();
            if (input.equalsIgnoreCase("quit")) stop = false;
            else{
                input = path.peek() + ";" + input;
                try{
                    response = handler.handleInput(input);
                    view = viewer.getView(response, path);
                }
                catch (Exception e){
                    viewer.getView("err", e).show();
                }
            }
        }

    }
}
