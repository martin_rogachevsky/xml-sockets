package client.views;

import client.XmlSocketAppClient;
import infrastructure.interfaces.IView;

public class RunView implements IView{
    /**
     * Method shows view and retrieves user input.
     *
     * @return - user input.
     */
    @Override
    public String show() {
        XmlSocketAppClient.startClient();
        System.out.println("                   Client is running.");
        System.out.println("====================================================");
        System.out.println(" Client was started. Press 'Enter' key to interrupt.");
        System.out.println("           Log will be displayed here.");
        System.out.println("====================================================");
        try{
            System.in.read();
        } catch (Exception e){
            System.out.println(e.getMessage());
        }
        return "quit";
    }

    /**
     * Method that allows view to get data from outside.
     *
     * @param param - data that used by view.
     */
    @Override
    public void build(Object param) {

    }

    /**
     * Method that resets view.
     */
    @Override
    public void clean() {

    }
}
