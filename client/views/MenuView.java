package client.views;

import infrastructure.interfaces.IView;

import java.util.Scanner;

public final class MenuView implements IView {
    /**
     * Method shows view and retrieves user input.
     *
     * @return - user input.
     */
    @Override
    public String show() {
        System.out.println("                      Client.");
        System.out.println("====================================================");
        System.out.println("'run' - start working with server.");
        System.out.println("====================================================");
        System.out.print("> ");
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }

    /**
     * Method that allows view to get data from outside.
     *
     * @param param - data that used by view.
     */
    @Override
    public void build(Object param) {

    }

    /**
     * Method that resets view.
     */
    @Override
    public void clean() {

    }
}
