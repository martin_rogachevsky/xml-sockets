package domain;

import java.util.ArrayList;

/**
 * Specialization domain class.
 */
public class Specialization {
    /**
     * Id of Specialization.
     */
    public int id;

    /**
     * Specialization name.
     */
    public String name;

    /**
     * Specialization type.
     */
    public EducationType type;

    /**
     * Students that are studying on this Specialization.
     */
    public ArrayList<Student> students;
}
