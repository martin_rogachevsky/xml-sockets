package domain;

public enum EducationType {
    Day,
    Evening,
    Distant
}
