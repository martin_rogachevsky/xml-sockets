package domain;

/**
 * Student domain class.
 */
public class Student {
    /**
     * Student id.
     */
    public int id;

    /**
     * Student name.
     */
    public String name;

    /**
     * Specializations that student is studying on.
     */
    public int[] specializations;
}
